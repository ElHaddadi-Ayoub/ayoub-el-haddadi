﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Properties_StringBuilder_lab4_
{  
    class ExtendedStudent : Student
    {
        public DateTime BirthDate { get; set; }
        public DateTime AdmissionDate { get; set; }
        public string FacultyName { get; set; }
        public string SpecialtyNumber { get; set; }
        public int GetSemesterNo {
            get
            {
                int semesters = 0;
                DateTime ad = AdmissionDate;
                while (DateTime.Compare(ad.Date, DateTime.Today.Date) < 0)
                {
                    semesters++;
                    ad = ad.AddMonths(5);
                }
                return semesters;
            }
        }
        public int GetCourseNo
        {
            get
            {
                if((GetSemesterNo <= 2))
                {
                    return 1;
                }
                else if ((GetSemesterNo % 2 == 0) && GetSemesterNo > 2)
                {
                    return GetSemesterNo / 2;
                }
                else
                {
                    return (GetSemesterNo / 2) + 1;
                }
            }
        }
        public string GetGroupName
        {
            get
            {
                var sb = new StringBuilder();
                sb.Append(SpecialtyNumber);
                sb.Append(" ");
                foreach (Match match in Regex.Matches(FacultyName, @"(\b\w)"))
                {
                    string result = match.Value.ToUpper();
                    sb.Append(result);
                }
                sb.Append("-");
                sb.Append((AdmissionDate.Year % 100) + 100);
                return sb.ToString();
            }
        }
        public int GetCurrentAge { 
            get
            {
                var age = DateTime.Today.Year - BirthDate.Year;
                if (BirthDate.Date > DateTime.Today.AddYears(-age)) age--;
                return age;
            } 
        }
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("\nSemester №: " + GetSemesterNo);
            sb.AppendLine("Course №: " + GetCourseNo);
            sb.AppendLine("Group name: " + GetGroupName);
            sb.AppendLine("Your current age is: : " + GetCurrentAge);
            return sb.ToString();          
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var es = new ExtendedStudent();
            Console.WriteLine("Enter your Admission Date: (e.g. MM/DD/YYYY) ");
            es.AdmissionDate = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("\nEnter Faculty name: ");
            es.FacultyName = Console.ReadLine();
            Console.WriteLine("\nEnter your Specialty number: ");
            es.SpecialtyNumber =Console.ReadLine();
            Console.WriteLine("\nEnter Birthday date: (e.g. MM/DD/YYYY)");
            es.BirthDate = DateTime.Parse(Console.ReadLine());
            Console.WriteLine(es);
            Console.ReadLine();
        }
    }
}
