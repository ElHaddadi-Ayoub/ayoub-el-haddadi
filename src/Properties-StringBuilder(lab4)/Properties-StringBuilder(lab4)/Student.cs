﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Properties_StringBuilder_lab4_
{
    class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public override string ToString()
        {
            return "\nFirstName: " + FirstName + "; LastName: " + LastName + "; Email: " + Email + "; PhoneNumber: " + PhoneNumber;
        }
    }
}
