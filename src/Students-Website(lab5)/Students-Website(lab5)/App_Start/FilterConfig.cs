﻿using System.Web;
using System.Web.Mvc;

namespace Students_Website_lab5_
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
