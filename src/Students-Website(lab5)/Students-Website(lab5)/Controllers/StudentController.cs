﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Students_Website_lab5_.Models;

namespace Students_Website_lab5_.Controllers
{
    public class StudentController : Controller
    {
        public static string fileName = "C:/Students/students.txt";
        public List<Student> students = new List<Student>();
        // GET: Student
        public ActionResult Index()
        {
            
            return View(fillCollection(students));
        }
        public static List<Student> fillCollection(List<Student> students)
        {

            if (!System.IO.File.Exists(fileName))
            {
                System.IO.File.WriteAllText(fileName, null);
            }
            string[] list = System.IO.File.ReadAllLines(fileName);
            students.Clear();
            foreach (string elem in list)
            {
                string[] fields = elem.Split(';');
                if (elem != "")
                {
                    var student = new Student();
                    student.FirstName = fields[0].Split(':')[1].Trim();
                    student.LastName = fields[1].Split(':')[1].Trim();
                    student.Email = fields[2].Split(':')[1].Trim();
                    student.PhoneNumber = fields[3].Split(':')[1].Trim();

                    student.BirthDate = DateTime.Parse(fields[4].Split(':')[1].Trim());
                    student.AdmissionDate = DateTime.Parse(fields[5].Split(':')[1].Trim());
                    student.FacultyName = fields[6].Split(':')[1].Trim();
                    student.SpecialtyNumber = fields[7].Split(':')[1].Trim();

                    students.Add(student);
                }
            }
            return students;
        }
        // GET: Student/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }
        public void rewriteFile(List<Student> students)
        {
            string[] list = new string[students.Count];
            int i = 0;
            foreach (var student in students)
            {
                list[i] = student.ToString();
                i++;
            }
            System.IO.File.WriteAllLines(fileName, list);
        }
        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var students = fillCollection(new List<Student>());
                var student = new Student();
                student.FirstName = collection["FirstName"];
                student.LastName = collection["LastName"];
                student.Email = collection["Email"];
                student.PhoneNumber = collection["PhoneNumber"];

                student.BirthDate = DateTime.Parse(collection["BirthDate"]);
                student.AdmissionDate = DateTime.Parse(collection["AdmissionDate"]);
                student.FacultyName = collection["FacultyName"];
                student.SpecialtyNumber = collection["SpecialtyNumber"];

                students.Add(student);
                rewriteFile(students);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Edit/5
        [HttpGet]
        public ActionResult Edit(int id)
        {
            fillCollection(students);
            if(id == 0 || id > students.Count)
            {
                return RedirectToAction("Index");
            }
            var student = students[id - 1];
            if (student != null)
            {
                ViewBag.id = id;
                return View(student);
            }
            return RedirectToAction("Index");
        }

        // POST: Student/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {   
            fillCollection(students);
            var student = new Student();
            student.FirstName = collection["FirstName"];
            student.LastName = collection["LastName"];
            student.Email = collection["Email"];
            student.PhoneNumber = collection["PhoneNumber"];

            student.BirthDate = DateTime.Parse(collection["BirthDate"]);
            student.AdmissionDate = DateTime.Parse(collection["AdmissionDate"]);
            student.FacultyName = collection["FacultyName"];
            student.SpecialtyNumber = collection["SpecialtyNumber"];

            int index = int.Parse(collection["id"]);
            students[index - 1] = student;
            rewriteFile(students);
            return RedirectToAction("Index");
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Student/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                fillCollection(students);
                if (id == 0 || id > students.Count)
                {
                    return RedirectToAction("Index");
                }
                int index = int.Parse(collection["id"]);
                students.RemoveAt(index - 1);
                rewriteFile(students);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
