﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Students_Website_lab5_.Models
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public DateTime BirthDate { get; set; }
        public DateTime AdmissionDate { get; set; }
        public string FacultyName { get; set; }
        public string SpecialtyNumber { get; set; }
        public int GetSemesterNo
        {
            get
            {
                int semesters = 0;
                DateTime ad = AdmissionDate;
                while (DateTime.Compare(ad.Date, DateTime.Today.Date) < 0)
                {
                    semesters++;
                    ad = ad.AddMonths(5);
                }
                return semesters;
            }
        }
        public int GetCourseNo
        {
            get
            {
                if ((GetSemesterNo <= 2))
                {
                    return 1;
                }
                else if ((GetSemesterNo % 2 == 0) && GetSemesterNo > 2)
                {
                    return GetSemesterNo / 2;
                }
                else
                {
                    return (GetSemesterNo / 2) + 1;
                }
            }
        }
        public string GetGroupName
        {
            get
            {
                var sb = new StringBuilder();
                sb.Append(SpecialtyNumber);
                sb.Append(" ");
                foreach (Match match in Regex.Matches(FacultyName, @"(\b\w)"))
                {
                    string result = match.Value.ToUpper();
                    sb.Append(result);
                }
                sb.Append("-");
                sb.Append((AdmissionDate.Year % 100) + 100);
                return sb.ToString();
            }
        }
        public int GetCurrentAge
        {
            get
            {
                var age = DateTime.Today.Year - BirthDate.Year;
                if (BirthDate.Date > DateTime.Today.AddYears(-age)) age--;
                return age;
            }
        }
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("FirstName: " + FirstName);
            sb.Append("; LastName: " + LastName);
            sb.Append("; Email: " + Email);
            sb.Append("; PhoneNumber: " + PhoneNumber);

            sb.Append("; BirthDate: " + BirthDate.ToString("MM/dd/yyyy"));
            sb.Append("; AdmissionDate: " + AdmissionDate.ToString("MM/dd/yyyy"));
            sb.Append("; FacultyName: " + FacultyName);
            sb.Append("; SpecialtyNumber:" + SpecialtyNumber);
            return sb.ToString();
        }
    }
}