﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student
{
    class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var student = new Student();
            Console.WriteLine("Input students's first name:");
            student.FirstName = Console.ReadLine();
            Console.WriteLine("Input students's last name:");
            student.LastName = Console.ReadLine();
            Console.WriteLine("Input students's phone number:");
            student.PhoneNumber = Console.ReadLine();
            Console.WriteLine("Input students's email:");
            student.Email = Console.ReadLine();
            Console.WriteLine("Student's info: \nFirst name: " + student.FirstName +
                              ", Last name: " + student.LastName +
                              ", Phone number: " + student.PhoneNumber +
                              ", Email: " + student.Email
                              );
            //Just for keep console open to see results
            Console.ReadLine();
        }
    }
}
