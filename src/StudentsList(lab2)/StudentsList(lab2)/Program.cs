﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentsList_lab2_
{
    class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public override string ToString()
        {
            return "\nFirstName: " + FirstName + "; LastName: " + LastName + "; Email: " + Email + "; PhoneNumber: " + PhoneNumber;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input how many students on the list: ");
            int studentsAmount = int.Parse(Console.ReadLine());
            var students = new List<Student>();
            for (int i = 0; i < studentsAmount; i++)
            {
                Console.WriteLine("\nStudent N° " + (i+1)+ " :");
                var student = new Student();
                Console.WriteLine("Input student's First name: ");
                student.FirstName = Console.ReadLine();
                Console.WriteLine("Input student's Last name: ");
                student.LastName = Console.ReadLine();
                Console.WriteLine("Input student's Email: ");
                student.Email = Console.ReadLine();
                Console.WriteLine("Input student's Phone number name: ");
                student.PhoneNumber = Console.ReadLine();
                students.Add(student);
            }
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }
            Console.ReadLine();
        }
    }
}
