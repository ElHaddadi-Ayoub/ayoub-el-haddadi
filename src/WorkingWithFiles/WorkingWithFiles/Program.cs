﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WorkingWithFiles
{
    class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public override string ToString()
        {
            return "FirstName: " + FirstName + "; LastName: " + LastName + "; Email:" + Email + "; PhoneNumber: " + PhoneNumber;
        }
    }
    class Program
    {
        
        static void Main(string[] args)
        {
            bool b = true;
            var students = new List<Student>();
            const string fileName = "students.txt";
            fillCollection(students, fileName);
            while (b)
            {
                Console.WriteLine("Enter -r to read students list");
                Console.WriteLine("Enter -a to add a student to the list");
                Console.WriteLine("Enter -e to edit a student to the list");
                Console.WriteLine("Enter -s to search for a student to the list");
                Console.WriteLine("Enter -d to delete a student to the list");
                Console.WriteLine("Enter \'exit\' to close the program");
                string request = Console.ReadLine();
                if(request == "-a")
                {
                    addStudent(students, fileName);
                }
                if (request == "-r")
                {
                    readFile(fileName);
                }
                if (request == "-e")
                {
                    edit(students, fileName);
                }
                if (request == "-s")
                {
                    search(students);
                }
                if (request == "-d")
                {
                    delete(students, fileName);
                }
                if (request == "exit")
                {
                    b = false;
                }
            }
        }
        static void addStudent(List<Student> students, string fileName)
        {
            var student = new Student();
            Console.WriteLine("Input student's First name: ");
            student.FirstName = Console.ReadLine();
            Console.WriteLine("Input student's Last name: ");
            student.LastName = Console.ReadLine();
            Console.WriteLine("Input student's Email: ");
            student.Email = Console.ReadLine();
            Console.WriteLine("Input student's Phone number: ");
            student.PhoneNumber = Console.ReadLine();
            students.Add(student);
            rewriteFile(students, fileName);
            Console.WriteLine("\nStudent added successfully to the list!\n");
        }
        static void rewriteFile(List<Student> students, String fileName)
        {
            string[] list = new string[students.Count];
            int i = 0;
            foreach(var student in students)
            {
                list[i] = student.ToString();
                i++;
            }
            System.IO.File.WriteAllLines(fileName, list);
        }
        static void fillCollection(List<Student> students, String fileName)
        {
            string[] list = System.IO.File.ReadAllLines(fileName);
            students.Clear();
            foreach(string elem in list)
            {
                string[] fields = elem.Split(';');
                if(elem != "")
                {
                    var student = new Student();
                    student.FirstName = fields[0].Split(':')[1].Trim();
                    student.LastName = fields[1].Split(':')[1].Trim();
                    student.Email = fields[2].Split(':')[1].Trim();
                    student.PhoneNumber = fields[3].Split(':')[1].Trim();
                    students.Add(student);
                }
            }
        }

        static void readFile(string fileName)
        {
            string[] list = System.IO.File.ReadAllLines(fileName);
            int i = 1;
            foreach (var ele in list)
            {
                if(ele != "")
                {
                    Console.WriteLine(i + ": " + ele);
                    i++;
                }
            }
            Console.WriteLine("\n");
        }

        static void edit(List<Student> students, String fileName)
        {
            readFile(fileName);
            Console.WriteLine("Input id of student to edit...");
            int index = int.Parse(Console.ReadLine());
            var student = new Student();
            Console.WriteLine("rewrite student's First name: ");
            student.FirstName = Console.ReadLine();
            Console.WriteLine("rewrite student's Last name: ");
            student.LastName = Console.ReadLine();
            Console.WriteLine("rewrite student's Email: ");
            student.Email = Console.ReadLine();
            Console.WriteLine("rewrite student's Phone number: ");
            student.PhoneNumber = Console.ReadLine();
            students[index-1] = student;
            rewriteFile(students, fileName);
            Console.WriteLine("\nStudent updated successfully to the list!\n");
        }

        static void search(List<Student> students)
        {
            Console.WriteLine("Search by first name input -fn");
            Console.WriteLine("Search by last name input -ln");
            Console.WriteLine("Search by email input -em");
            Console.WriteLine("Search by phone number input -pn");
            string searchBy = Console.ReadLine();
            if(searchBy == "-fn")
            {
                Console.WriteLine("Input first-name in mind...");
                string c = Console.ReadLine();
                var result = students.Find(x => x.FirstName.Contains(c));
                Console.WriteLine("Results of search...");
                Console.WriteLine(result + "\n");
            }
            if (searchBy == "-ln")
            {
                Console.WriteLine("Input last-name in mind...");
                string c = Console.ReadLine();
                var result = students.Find(x => x.LastName.Contains(c));
                Console.WriteLine("Results of search...");
                Console.WriteLine(result + "\n");
            }
            if (searchBy == "-em")
            {
                Console.WriteLine("Input email in mind...");
                string c = Console.ReadLine();
                var result = students.Find(x => x.Email.Contains(c));
                Console.WriteLine("Results of search...");
                Console.WriteLine(result + "\n");
            }
            if (searchBy == "-pn")
            {
                Console.WriteLine("Input Phone-Number in mind...");
                string c = Console.ReadLine();
                var result = students.Find(x => x.PhoneNumber.Contains(c));
                Console.WriteLine("Results of search...");
                Console.WriteLine(result + "\n");
            }
        }

        static void delete(List<Student> students, string fileName)
        {
            readFile(fileName);
            Console.WriteLine("Input id of student to delete...");
            int index = int.Parse(Console.ReadLine());
            students.RemoveAt(index-1);
            rewriteFile(students, fileName);
            Console.WriteLine("\nStudent deleted successfully to the list!\n");
        }
    }
}
